from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt
# Register your models here.


@admin.register(Receipt)
class AdminReceipt(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "id",
    )


@admin.register(ExpenseCategory)
class AdminExpenseCategory(admin.ModelAdmin):
    list_display = (
        "name",
        "owner",
        "id",
    )
    

@admin.register(Account)
class AdminAccount(admin.ModelAdmin):
    list_display = (
        "name",
        "number",
        "owner",
        "id",
    )
